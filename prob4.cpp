#include <iostream>
#include <cmath>

bool is_prime(unsigned int a)
{
    bool result;
    unsigned int root = static_cast<unsigned int>(std::sqrt(a));

    if (1 < a && a <= 3) {
        result = true;
    }
    else if (a % 3 == 0 || a % 2 == 0) {
        result = false;
    } 
    else {
        for (unsigned int i = 5; i <= root; i += 6) {
            if (a % i == 0 || a % (i + 2) == 0) {
                result = true;
                break;
            }
        }
        result = true;
    }

    return result;
}

int main(void)
{
    unsigned int number = 0;
    std::cout << "Input limit" << std::endl;
    std::cin >> number;

    for (unsigned int i = 1; i <= number; i++) {
        if (true == is_prime(i)) {
            std::cout << i << std::endl;
        }
    }
    return 0;
}
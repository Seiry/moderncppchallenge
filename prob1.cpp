#include <iostream>

int main(void)
{
    unsigned int number;
    unsigned int sum;
    std::cout << "Input number" << std::endl;
    std::cin >> number;

    for (unsigned int i = 0; i < number; i++) {
        if (number % i == 0 || number % 5 == 0) {
            sum += i;
        }
    }
    std::cout << number << std::endl;

    return 0;
}
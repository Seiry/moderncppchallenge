CXX := g++-9
N := 1
TARGET := prob${N}

CXXFLAGS := -Wall -std=c++17

.PHONY:all clean
all:${TARGET}

${TARGET}.o:${TARGET}.cpp
	${CXX} -c $< ${CXXFLAGS}

${TARGET}:${TARGET}.o
	${CXX} -o $@ $< ${CXXFLAGS}

clean:
	rm -rf *.o 
	rm -rf prob[0-9]
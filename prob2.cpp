#include <iostream>

unsigned int gcd(unsigned int a, unsigned int b)
{
    unsigned int r = 0;
    while (b != 0)
    {
        r = a % b;
        a = b;
        b = r;
    }
    return a;
}

int main(void)
{
    unsigned int num1,num2;

    std::cout << "Input number" << std::endl;
    std::cin >> num1;
    std::cin >> num2;

    std::cout << "gcd is " << gcd(num1,num2) << std::endl;

    return 0;
}